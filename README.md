# tpm2-algtest-nspawn

NAME
----

`tpm2-algtest-nspawn` - systemd-nspawn wrapper for
[tpm2-algtest](https://github.com/crocs-muni/tpm2-algtest)


SYNPOSIS
--------

    tpm2-algtest-nspawn -h
    tpm2-algtest-nspawn [-i image] [-f]


DESCRIPTION
-----------

This script runs tpm2-algtest suite inside a systemd-nspawn(1) container.
It is useful when running under `root` directly is not desirable for various
reasons.

Aside from dependencies of tpm2-algtest, the following are required:

- systemd-based Linux operating system,
- machinectl(1) with importd support if the system has unmerged `/bin` or
  `/sbin` (like Gentoo)

### Overview

The script runs in three phases:

1.  tpm2-algtest repository is cloned or updated from Git.
2.  tpm2-algtest sources are compiled under a non-root user. This is done
    by calling the script again with `compile` argument.
3.  The script is re-run again in a systemd-nspawn(1) container where TPMs
    are made available.
4.  Results are uploaded to IS MU.

### Running on a system with merged `/bin` and `/sbin`

The following should be enough:

    # ./tpm2-algtest-nspawn

### Running on a system with unmerged `/bin` and `/sbin`

Using this script on such a system requires a machine image. The script has
been teste with Ubuntu, which can be obtained here:
https://cloud-images.ubuntu.com/

1.  Select a release and navigate into it's `current` subdirectory. From here,
    download a TAR file for your architecture (e.g. amd64) with `-root` suffix.

2.  Import the image as some easily remembered name, e.g. `ubuntu`:

      # machinectl import-tar TAR_FILE ubuntu

    machinectl(1) supports `pull-tar` command which can download the tar itself,
    but you will have to set up Ubuntu's (or other distribution's) public keys
    yourself, as this command attempts to verify the signatures.

3. Start the container and install dependencies for tpm2-algtest:

      # systemd-nspawn -D /var/lib/machines/ubuntu --bind-ro=/run/systemd

      root@ubuntu# apt update
      root@ubuntu# apt install build-essential cmake tpm2-tools libtss2-dev libssl-dev

4.  Exit the container and run the script:

      # ./tpm2-algtest-nspawn

    If you imported the machine with a different name than `ubuntu`,
    either change it in `config` file or use `-i IMAGE` option for the script.


OPTIONS
-------

* `-i` <IMAGE>:
  Use <IMAGE> instead of (default) `ubuntu`. See [DESCRIPTION][] on when this
  option is useful.

* `-u`, `-U`
  Enable (default) or disable tpm2-algtest repository update.
  Do not use `-U` for the first time when running the script, as the repository
  must be cloned to be used.

* `-x`, `-X`
  Enable (default) or disable uploading of the result.

* `-f`:
  Do not check user identity. The script will not complain if not run by root.
  It will also not complain when root attempts to compile sources directly.


CONFIGURATION
-------------

Configuration file for this script is itself a shell script where only variables
are expected to be set.

See comments in `config` for more details.


REFERENCES
----------

- [tpm2-algtest](https://github.com/crocs-muni/tpm2-algtest) repository
